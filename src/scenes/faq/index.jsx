import { Box, useTheme, Typography } from "@mui/material";
import Header from "../../components/Header";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore"
import { tokens } from "../../theme";

const FAQ = () => {
	const theme = useTheme()
	const colors = tokens(theme.palette.mode)

	return(
		<Box m="20 px">
			<Header title="FAQ" subtitle="Frequently Asked Questions Page" />

			<Accordion defaultExpanded>
				<AccordionSummary expandIcon={<ExpandMoreIcon/>}>
					<Typography color={colors.greenAccent[500]} variant="h5">
						An Important Questions
					</Typography>
				</AccordionSummary>
				<AccordionDetails>
					<Typography>
						Lorem, ipsum dolor sit amet consectetur adipisicing elit. Veniam totam recusandae similique, veritatis autem officia id optio, placeat, iste ipsa dolorem quae. Voluptates cupiditate aspernatur totam consectetur ut consequuntur ipsum!
					</Typography>
				</AccordionDetails>
			</Accordion>

			<Accordion>
				<AccordionSummary expandIcon={<ExpandMoreIcon/>}>
					<Typography color={colors.greenAccent[500]} variant="h5">
						Another Important Questions
					</Typography>
				</AccordionSummary>
				<AccordionDetails>
					<Typography>
						Lorem, ipsum dolor sit amet consectetur adipisicing elit. Veniam totam recusandae similique, veritatis autem officia id optio, placeat, iste ipsa dolorem quae. Voluptates cupiditate aspernatur totam consectetur ut consequuntur ipsum!
					</Typography>
				</AccordionDetails>
			</Accordion>

			<Accordion> 
				<AccordionSummary expandIcon={<ExpandMoreIcon/>}>
					<Typography color={colors.greenAccent[500]} variant="h5">
						Your Favorite Question
					</Typography>
				</AccordionSummary>
				<AccordionDetails>
					<Typography>
						Lorem, ipsum dolor sit amet consectetur adipisicing elit. Veniam totam recusandae similique, veritatis autem officia id optio, placeat, iste ipsa dolorem quae. Voluptates cupiditate aspernatur totam consectetur ut consequuntur ipsum!
					</Typography>
				</AccordionDetails>
			</Accordion>

			<Accordion>
				<AccordionSummary expandIcon={<ExpandMoreIcon/>}>
					<Typography color={colors.greenAccent[500]} variant="h5">
						A Random Questions
					</Typography>
				</AccordionSummary>
				<AccordionDetails>
					<Typography>
						Lorem, ipsum dolor sit amet consectetur adipisicing elit. Veniam totam recusandae similique, veritatis autem officia id optio, placeat, iste ipsa dolorem quae. Voluptates cupiditate aspernatur totam consectetur ut consequuntur ipsum!
					</Typography>
				</AccordionDetails>
			</Accordion>

			<Accordion >
				<AccordionSummary expandIcon={<ExpandMoreIcon/>}>
					<Typography color={colors.greenAccent[500]} variant="h5">
						A Final Questions
					</Typography>
				</AccordionSummary>
				<AccordionDetails>
					<Typography>
						Lorem, ipsum dolor sit amet consectetur adipisicing elit. Veniam totam recusandae similique, veritatis autem officia id optio, placeat, iste ipsa dolorem quae. Voluptates cupiditate aspernatur totam consectetur ut consequuntur ipsum!
					</Typography>
				</AccordionDetails>
			</Accordion>
		</Box>
	)
}

export default FAQ